(function(window, $) {

	// timer
	var ClassTimer = (function() {
		return function() {
			var _date = new Date();
      _date.setMonth(1);
      _date.setDate(9);
      _date.setHours(0);
      _date.setMinutes(0);
      _date.setSeconds(0);
      $('#counter').countdown({
          startTime: _date,
          // stepTime: ,
          digitImages: 1,
          digitWidth: 38,
          digitHeight: 53,
          image: "images/digits.png"
      });
		};
	})();

  var ClassPopup = (function() {
    function app() {
      var that = this;
      this.popup = $(".popup");
      $(".popup-close, .shadow").on("click", function() {
        that.close();
      });
    }

    app.prototype.show = function(txt) {
      this.popup.find("> p").html(txt);
      $body.addClass("-popup");
    };

    app.prototype.close = function() {
      $body.removeClass("-popup");
    };

    return app;
  })();

  var $body, Timer, Popup;
//Спасибо за заявку. Мы свяжемся с вами в ближайшее время.
	$(function() {
		Timer = new ClassTimer();
    Popup = new ClassPopup();
    $body = $("body");

    $(".formsign-send").on("click", function() {
      Popup.show("Спасибо за заявку. Мы свяжемся с вами в ближайшее время.");
      return false;
    });
	});

})(window, jQuery);
